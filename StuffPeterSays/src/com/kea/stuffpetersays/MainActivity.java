package com.kea.stuffpetersays;

import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.Menu;
import android.view.View;
import android.view.Window;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.welcome);
	}
	
	public void onClickMain_menu(View v)
	{
		setContentView(R.layout.main_menu);
	}

	public void onClick_Weblink(View v)
	{
		try {
		    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/100000542752561"));
		    startActivity(intent);
		} catch(Exception e) {
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/stuffpetersays")));
			}
        }

       
	
}
